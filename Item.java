/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avaliacao2.gustavoleao.lab;

/**
 *
 * @author 181510004
 * @version 1.0.1
 *
 * 
 */
public class Item {
    private int quantidade;
    private String nomeProduto;
    private boolean comprado;

    public Item(int quantidade, String nomeProduto) {
        this.quantidade = quantidade;
        this.nomeProduto = nomeProduto;
        this.comprado = false;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public boolean isComprado() {
        return comprado;
    }

    public void setComprado(boolean comprado) {
        this.comprado = comprado;
    }
    
    
}
