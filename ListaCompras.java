/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avaliacao2.gustavoleao.lab;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author 181510004
 * @version 1.0.12
 * 
 */
public class ListaCompras {
    static final int TAMANHO = 5;
    static Item lista[] = new Item[TAMANHO];
    static int indice = 0;
    private static final String[] PRODUTOS = new String[]{
        "Açúcar",
"Sal",
"Arroz",
"Feijão",
"Farinha",
"Macarrão",
"Café",
"Leite",
"Óleo",
"Temperos",
"Molho de tomate",
"Queijo ralado",
"Ovos",
"Fermento",
"Pães",
"Carnes",
"Iogurte",
"Margarina ou manteiga",
"Maisena",
"Biscoitos",
"Hortaliças (alface, tomate, cebola e outros)",
"Sabão em pedra",
"Sabão em pó",
"Detergente",
"Desinfetante",
"Amaciante",
"Lustra-móveis",
"Álcool em gel",
"Água sanitária",
"Inseticida",
"Esponja de pia",
"Esponja de aço",
"Sacos de lixo",
"Luvas plásticas",
"Flanelas",
"Sabonete",
"Creme dental",
"Escova de dente",
"Fio dental",
"Absorventes",
"Barbeador descartável",
"Creme de barbear",
"Algodão",
"Desodorante",
"Shampoo e condicionador",
"Papel higiênico",
"Água oxigenada",
"Gaze",
"Hastes flexíveis",
"Esparadrapo",
"Curativos",
"Papel alumínio",
"Papel filme",
"Papel toalha",
"Guardanapo de papel",
"Fósforo",
"Velas",
"Lâmpadas",
"Fita isolante",
"Fita crepe"            
    };
     
    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        String[] opcoes = new String[]{
            "1 - Inserir Item",
            "2 - Retirar Item",
            "3 - Listar  Item",
            "4 - Sair"            
        };
        
        switch(lerMenu(opcoes)){
            case 1: inserir(); menu(); break;
            case 2: retirar(); menu(); break;
            case 3: listarProduto(); menu(); break;
        }
    }

    public  static int lerMenu(String[] opcoes) {
        System.out.println("");
        escreve("Menu:");
        for (String opc: opcoes){
            escreve(opc);
        }
        System.out.print("Escolha: ");
        int opc = new Scanner(System.in).nextInt();
        return opc;
    }

    public  static void inserir() {
        for(int i = 0; i < TAMANHO; i++){
            Item item = new Item();
            item.setNomeProduto(lerProduto("Digite o nome do produto:"));
            item.setQuantidade(lerQuantidade("Digite a quantidade do produto:"));
            lista[i] = item;
        }
    }
    
    public static int lerQuantidade(String texto){
        System.out.println("");
        System.out.print(texto);
        int quant = 0;
        
        do{
            quant = new Random().nextInt(100);
            System.out.print(quant);
            if(quant <= 0)System.out.print("Digite uma quantidade valida [acima de 0]:");
        }while(quant <= 0);
        return quant;
    }
    
    public static String lerProduto(String texto){
        System.out.println("");
        System.out.print(texto);
        String produto = "";
        do{
            produto = PRODUTOS[new Random().nextInt(PRODUTOS.length-1)];
            System.out.print(produto);
            if(produto == null || produto.equals("") || produto.equals(" "))System.out.print("Digite um produto válido:");
        }while(produto == null || produto.equals("") || produto.equals(" "));
        return produto;
    }


    public static void retirar() {
        for(int i = 0; i < TAMANHO; i++){
            escreve("Deseja compraro o item "+lista[i].getNomeProduto()+"? [s/n] ");
            char opc = new Scanner(System.in).nextLine().toLowerCase().charAt(0);
            if(opc == 's')lista[i].setComprado(true);
            else lista[i].setComprado(false);
        }
    }

    public static void listarProduto() {
        indice = 1;
        linha();
        escreve("Faltam serem comprados:");
        for(Item item: lista){
            escreve("O item: "+item.getNomeProduto()+" \t | \t Comprado: "+item.isComprado());
            if(item.isComprado()){
                indice++;
            }
        }
        System.out.println("Total de produtos a serem comprados: "+indice);
    }

    public static void linha() {
        for(int i = 0; i < 100; i++) System.out.print("#");
    }

    public static void escreve(String texto) {
        System.out.println(texto);
    }
}
